/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import * as Infos from './infos.js';
import { Gio, GLib, GObject, Gtk } from './gi.js';
const ByteArray = imports.byteArray;

const BASE_INDENTATION = 4;

String.prototype.indent = function(i = 1) {
    return this.replace(/^/gm, " ".repeat(BASE_INDENTATION * i));
};

class Context {
    constructor() {
        this._stack = [{ text: "", multiline: true, separator: ";\n" }];
        this._links = new Set();
    }

    get current() {
        return this._stack[this._stack.length - 1];
    }

    push(start = "", multiline = false, separator = ",") {
        this._stack.push({ multiline, separator, start, text: "" });
    }

    append(text, comment) {
        if (this.current.multiline)
            this.current.text += "\n";
        else if (this.current.text)
            this.current.text += " ";
        this.current.text += text;
        if (comment && !this.current.multiline)
            this.current.text += ` /* ${comment} */`;
        this.current.text += this.current.separator;
        if (comment && this.current.multiline)
            this.current.text += ` /* ${comment} */`;
    }

    pop(end = "") {
        let { multiline, start, text } = this._stack.pop();
        // Remove the last comma, but not the comment which may be positioned after.
        text = text.trimEnd().replace(/,( \/\*.*)?$/g, "$1");
        if (text && multiline)
            text = text.indent() + "\n";
        this.append(start + text + end);
    }

    addLink(text) {
        this._links.add(text);
    }

    getLinks() {
        return [...this._links];
    }

    getSources() {
        return this._stack[0].text.trim();
    }
}

Infos.FunctionInfo.prototype.sourcify = function(context, asMethod) {
    let ret = this.returnString;
    let out = this.outArgs.map(info => `${info.base.get_name()} ${info.string}`);
    if (ret)
        out.unshift(ret);

    let outComment = "";
    if (out.length == 1)
        outComment = `/* ${out[0]} */\n`;
    else if (out.length > 1)
        outComment = `/* [${out.join(", ")}] */\n`;

    let content = "";
    if (this.can_throw_gerror())
        content = `\n${" ".repeat(BASE_INDENTATION)}throw new Error();\n`;

    let start = asMethod ?
        `${outComment}${this.isStatic ? "static " : ""}${this.base.get_name()}(` :
        `${outComment}${this.base.getPath()} = function(`;

    context.push(start, true);
    this.inArgs.forEach(info => info.sourcify(context));
    context.pop(`) {${content}}`);
};

Infos.CallbackInfo.prototype.sourcify = function(context, asMethod) {
    let ret = this.returnString;
    let out = this.outArgs.map(info => `${info.base.get_name()} ${info.string}`);
    if (ret)
        out.unshift(ret);

    let outComment = "";
    if (out.length == 1)
        outComment = `/* ${out[0]} */\n`;
    else if (out.length > 1)
        outComment = `/* [${out.join(", ")}] */\n`;

    let funcName = this.base.get_name()[0].toLowerCase() + this.base.get_name().slice(1);

    context.current.separator = "";
    context.push(`${outComment}function ${funcName}(`, true);
    this.inArgs.forEach(info => info.sourcify(context));
    context.pop(`) {}`);
};

Infos.SignalInfo.prototype.sourcify = function(context) {
    context.push(`'${this.base.get_name()}': {`, true);
    context.append(`flags: ${this.flagsPath}`);
    if (this.has('arg')) {
        context.push(`param_types: [`, true);
        this.getInfos('arg').forEach(info => context.append(`/* ${info.base.get_name()}: */ ${info.typePath}`));
        context.pop("]");
    }
    if (this.hasReturn)
        context.append(`return_type: ${this.getInfo('return_type').path}`);
    context.pop("}");
};

Infos.VFuncInfo.prototype.sourcify = function(context) {
    let ret = this.returnString;
    let out = this.outArgs.map(info => `${info.base.get_name()} ${info.string}`);
    if (ret)
        out.unshift(ret);

    let outComment = "";
    if (out.length == 1)
        outComment = `/* ${out[0]} */\n`;
    else if (out.length > 1)
        outComment = `/* [${out.join(", ")}] */\n`;

    let content = "";
    if (this.can_throw_gerror())
        content = `\n${" ".repeat(BASE_INDENTATION)}throw new Error();\n`;

    context.push(`${outComment}vfunc_${this.base.get_name()}(`, true);
    this.inArgs.forEach(info => info.sourcify(context));
    context.pop(`) {${content}}`);
};

Infos.EnumInfo.prototype.sourcify = function(context) {
    context.push(`${this.base.getPath()} = {`, true);
    this.getInfos('value').forEach(info => info.sourcify(context));
    this.getInfos('method').forEach(info => info.sourcify(context, true));
    context.pop("}");
};

Infos.StructInfo.prototype.sourcify = function(context) {
    context.push(`${this.base.getPath()} = class {`, true, "\n");
    context.push(`constructor ({`, true);
    this.getInfos('field').forEach(info => info.sourcify(context, false));
    context.pop(`}) {}`);
    this.getInfos('field').forEach(info => info.sourcify(context, true));
    this.getInfos('method').forEach(info => info.sourcify(context, true));
    context.pop("}");
};

Infos.UnionInfo.prototype.sourcify = Infos.StructInfo.prototype.sourcify;

Infos.ObjectInfo.prototype.sourcify = function(context) {
    // XXX: What is an object constant?
    if (this.has('constant')) {
        log(`${this.base.get_name()} has constants:`);
        log(this.getInfos('constant').map(info => info.base.get_name()));
    }

    context.push(`${this.base.getPath()} = GObject.registerClass(`, false);
    context.push("{", true);
    {
        context.append(`GTypeName: '${this.get_type_name()}'`);
        if (this.get_abstract())
            context.append(`GTypeFlags: GObject.TypeFlags.ABSTRACT`);
        if (this.has('interface')) {
            context.push("Implements: [", false);
            this.getBases('interface').forEach(base => context.append(`@@${base.getPath()}@@`));
            context.pop("]");
        }
        if (this.has('property')) {
            context.push(`Properties: {`, true);
            this.getInfos('property').forEach(info => {
                info.sourcify(context);
            });
            context.pop("}");
        }
        if (this.has('signal')) {
            context.push(`Signals: {`, true);
            this.getInfos('signal').forEach(info => {
                info.sourcify(context);
            });
            context.pop("}");
        }
    }
    context.pop("}");
    let baseClass = this.get_fundamental() ? "FUNDAMENTAL" : this.get_parent()?.getPath();
    context.push(`class extends @@${baseClass}@@ {`, true, "\n");
    {
        this.allFunctions.flat().forEach(info => {
            info.sourcify(context, true);
        });
    }
    context.pop("}");
    context.pop(')');
};

Infos.InterfaceInfo.prototype.sourcify = function(context) {
    // XXX: What is an interface constant?
    if (this.has('constant')) {
        log(`${this.base.get_name()} has constants:`);
        log(this.getInfos('constant').map(info => info.base.get_name()));
    }

    context.push(`${this.base.getPath()} = GObject.registerClass(`, false);
    context.push("{", true);
    {
        context.append(`GTypeName: '${this.get_type_name()}'`);
        if (this.has('prerequisite')) {
            context.push("Requires: [", false);
            this.getBases('prerequisite').forEach(base => context.append(`@@${base.getPath()}@@`));
            context.pop("]");
        }
        if (this.has('property')) {
            context.push(`Properties: {`, true);
            this.getInfos('property').forEach(info => {
                info.sourcify(context);
            });
            context.pop("}");
        }
        if (this.has('signal')) {
            context.push(`Signals: {`, true);
            this.getInfos('signal').forEach(info => {
                info.sourcify(context);
            });
            context.pop("}");
        }
    }
    context.pop("}");
    context.push(`class extends GObject.Interface {`, true, "\n");
    {
        this.allFunctions.flat().forEach(info => {
            info.sourcify(context, true);
        });
    }
    context.pop("}");
    context.pop(')');
};

Infos.ArgInfo.prototype.sourcify = function(context) {
    context.append(this.base.get_name(), this.string);
};

Infos.ConstantInfo.prototype.sourcify = function(context) {
    let value = this.isString ? `"${this.value}"` : this.value;
    context.append(`${this.base.getPath()} = ${value}`);
};

Infos.FieldInfo.prototype.sourcify = function(context, asAccessor) {
    if (asAccessor) {
        if (this.readable) {
            let outComment = `/* ${this.string} */\n`;
            context.append(`${outComment}get ${this.base.get_name()}() {}`);
        }

        if (this.writable)
            context.append(`set ${this.base.get_name()}(${this.base.get_name()}) {}`);
    } else if (this.writable) {
        context.append(`${this.base.get_name()} /* ${this.string} */`);
    }
};

Infos.PropertyInfo.prototype.sourcify = function(context) {
    let { funcPath, typePath, flagsPath } = this.pspecPaths;
    let noDefault = funcPath.endsWith("boxed") || funcPath.endsWith("object");
    let defaultValue = !noDefault && (typeof this.pspec.default_value == 'string' ? `'${this.pspec.default_value}'` : this.pspec.default_value);
    context.push(`'${this.base.get_name()}': ${funcPath}(`, true);
    context.append(`'${this.base.get_name()}', '${this.pspec.nick}', '${this.pspec.blurb}'`);
    context.append(`${flagsPath}, ${typePath ? typePath + (noDefault ? "" : ", ") : ""}${noDefault ? "" : defaultValue}`);
    context.pop(")");
};

Infos.ValueInfo.prototype.sourcify = function(context) {
    context.append(`${this.base.get_name().toUpperCase()} : ${this.get_value()}`);
};

const LinkTag = GObject.registerClass({
    Properties: {
        'link': GObject.ParamSpec.string(
            'link', "Link", "The link",
            GObject.ParamFlags.READWRITE, null
        ),
    },
}, class extends Gtk.TextTag {});

let hasHighlightProgram = true;

export const SourceView = GObject.registerClass({
    Signals: {
        'link-activated': { param_types: [GObject.TYPE_STRING, GObject.TYPE_STRING] },
    },
}, class extends Gtk.TextView {
    _init(params) {
        super._init(Object.assign({
            monospace: true, editable: false,
            leftMargin: 12, rightMargin: 12,
            topMargin: 12, bottomMargin: 12,
            wrapMode: Gtk.WrapMode.WORD_CHAR,
        }, params));

        let motionController = new Gtk.EventControllerMotion();
        this.add_controller(motionController);
        motionController.connect('motion', (controller_, x, y) => {
            [x, y] = this.window_to_buffer_coords(Gtk.TextWindowType.TEXT, x, y);

            let [hasText, iter] = this.get_iter_at_location(x, y);
            let link = hasText ? (iter.get_tags().find(tag => tag instanceof LinkTag)?.link ?? null) : null;

            if (link && link.match(/\w+\.\w+/)) {
                this.set_cursor_from_name('pointer');
                this._pointedLink = link;
            } else if (this._pointedLink) {
                this.set_cursor_from_name('text');
                this._pointedLink = null;
            }
        });

        let clickController = new Gtk.GestureClick();
        this.add_controller(clickController);
        clickController.connect('released', () => {
            if (this._pointedLink)
                this.emit('link-activated', ...this._pointedLink.split('.'));
        });
    }

    _addLinkTags(links) {
        [...links].forEach(link => {
            let iter = this.buffer.get_start_iter();
            while (!iter.is_end()) {
                let [success, matchStartIter, matchEndIter] =
                    iter.forward_search(link, Gtk.TextSearchFlags.TEXT_ONLY, null);

                if (success) {
                    let tag = new LinkTag({ link });
                    this.buffer.tagTable.add(tag);
                    this.buffer.apply_tag(tag, matchStartIter, matchEndIter);
                    iter = matchEndIter;
                } else {
                    break;
                }
            }
        });
    }

    _extractLinks(text) {
        let links = new Set();

        text = text.replace(/@@(.*)@@/g, (match, p1, offset) => {
            links.add(p1);
            return p1;
        });

        return [text, links];
    }

    _showText(bytes) {
        try {
            this.buffer.text = ByteArray.toString(ByteArray.fromGBytes(bytes));
        } catch(e) {}
    }

    _showHighlight(text) {
        this.buffer.text = '';
        let [source, links] = this._extractLinks(text);
        let bytes = GLib.Bytes.new(source);

        if (!hasHighlightProgram) {
            this._showText(bytes);
            return;
        }

        let color = this.get_style_context().get_color();
        let dark = (color.red + color.green + color.blue) > 3 / 2;

        try {
            Gio.Subprocess.new(
                [
                    'highlight', '--fragment', '--out-format=pango',
                    `--style=${dark ? 'edit-vim-dark' : 'edit-kwrite'}`,
                    '--syntax=js',
                ],
                Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE
            ).communicate_async(bytes, null, (subprocess, result) => {
                try {
                    let [success_, stdout, stderr] = subprocess.communicate_finish(result);
                    if (subprocess.get_exit_status() != 0 || !stdout) {
                        if (stderr)
                            log(ByteArray.toString(stderr.get_data()));

                        return;
                    }

                    let markup = ByteArray.toString(stdout.get_data());
                    // Remove the global font attributes.
                    markup = markup.slice(markup.indexOf('>') + 1, markup.lastIndexOf('<')).trim();

                    this.buffer.insert_markup(this.buffer.get_start_iter(), markup, -1);
                } catch(e) {
                    this._showText(bytes);
                }

                this._addLinkTags(links);
            });
        } catch(e) {
            log("For syntax highlighting, install the “highlight” program");
            hasHighlightProgram = false;
            this._showText(bytes);
        }
    }

    showInfo(info) {
        let context = new Context();
        info.sourcify?.(context);
        this._showHighlight(context.getSources());
    }

    showToplevel(namespaceObject) {
        let context = new Context();
        namespaceObject.constants.forEach(info => {
            info.sourcify?.(context);
        });
        namespaceObject.functions.forEach(info => {
            info.sourcify?.(context, false);
        });
        this._showHighlight(context.getSources());
    }
});
