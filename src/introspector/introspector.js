/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { Gio, GLib, Gir, GObject, Gtk } from './gi.js';
import { Info } from './infos.js';
import { SourceView } from './sources.js';
import { SearchBox, SearchSlider } from './search.js';

// Namespaces info items are hosted by NamespaceObject instead of NamespaceModel because
// they are necessary to display constants and toplevel functions.
const NamespaceObject = GObject.registerClass({
    Properties: {
        'string': GObject.ParamSpec.string(
            'string', "String", "The namespace string",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, null
        ),
    },
}, class extends GObject.Object {
    get infos() {
        if (!this._infos) {
            this._infos = [];

            try {
                let [namespace, version] = this.string.split('-');

                if (imports.gi.versions[namespace] && imports.gi.versions[namespace] != version)
                    return this._infos;

                imports.gi.versions[namespace] = version;
                void imports.gi[namespace];

                let repository = Gir.Repository.get_default();
                this._infos = Array
                    .from({ length: repository.get_n_infos(namespace) }, (e_, index) => repository.get_info(namespace, index))
                    .map(base => Info.newForBase(base));
            } catch(e) {}
        }

        return this._infos;
    }

    get constants() {
        return this.infos.filter(info => info.base.get_type() == Gir.InfoType.CONSTANT);
    }

    get functions() {
        return this.infos.filter(info => info.base.get_type() == Gir.InfoType.FUNCTION);
    }
});

// Not to use GtkFilterListModel because it queries items at startup, while the list row is not
// expanded. Querying namespace items implies to load the library so it must be done on demand.
const NamespaceModel = GObject.registerClass({
    Implements: [Gio.ListModel],
    Properties: {
        'filter': GObject.ParamSpec.object(
            'filter', "Filter", "The filter",
            GObject.ParamFlags.READWRITE, Gtk.Filter.$gtype
        ),
        'namespace': GObject.ParamSpec.object(
            'namespace', "Namespace", "The namespace object this describes",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, NamespaceObject.$gtype
        ),
    },
}, class extends GObject.Object {
    get items() {
        if (!this._items) {
            this._items = this.namespace.infos.filter(info => {
                if (this.filter && !this.filter.match(info))
                    return false;

                switch (info.base.get_type()) {
                    case Gir.InfoType.CONSTANT:
                    case Gir.InfoType.FUNCTION:
                        return false;
                    case Gir.InfoType.STRUCT:
                        return !Gir.struct_info_is_gtype_struct(info.base) && !info.base.get_name().toLowerCase().includes('private');
                    default:
                        return true;
                }
            });
        }

        return this._items;
    }

    vfunc_get_item_type() {
        return Info.$gtype;
    }

    vfunc_get_item(position) {
        return this.items[position] ?? null;
    }

    vfunc_get_n_items() {
        return this.items.length;
    }

    get filter() {
        return this._filter ?? null;
    }

    set filter(filter) {
        if (this._filter == filter)
            return;

        this._filter?.disconnect(this._filterHandler);
        this._filterHandler = filter?.connect('changed', () => {
            let removed = this._items.length ?? 0;
            delete this._items;
            this.items_changed(0, removed, this.items.length);
        });

        this._filter = filter;
        this.notify('filter');
    }
});

export default GObject.registerClass({
}, class Introspector extends Gtk.Paned {
    _init(params) {
        this._namespaceList = new Gio.ListStore({ itemType: NamespaceObject.$gtype });

        let treeListModel = Gtk.TreeListModel.new(
            new Gtk.FilterListModel({
                filter: new Gtk.StringFilter({
                    expression: Gtk.PropertyExpression.new(NamespaceObject.$gtype, null, 'string'),
                }),
                model: this._namespaceList,
            }),
            false, false,
            item => {
                if (item instanceof NamespaceObject)
                    return new NamespaceModel({
                        filter: new Gtk.StringFilter({
                            expression: Gtk.PropertyExpression.new(Gtk.StringObject.$gtype, null, 'string'),
                        }),
                        namespace: item,
                    });

                return null;
            },
        );

        this._history = [];
        this._history.currentIndex = -1;

        let upButton = new Gtk.Button({
            hasFrame: false, iconName: 'go-up-symbolic',
            sensitive: false, tooltipText: "Up",
        });
        upButton.connect('clicked', () => {
            let namespaceRow = this._listView.model.selectedItem.get_parent();
            this._select(namespaceRow.item.string);
        });
        let prevButton = new Gtk.Button({
            hasFrame: false, iconName: 'go-previous-symbolic',
            sensitive: false, tooltipText: "Back",
        });
        prevButton.connect('clicked', () => {
            let { namespaceString, infoName } = this._history[--this._history.currentIndex];
            this._history.locked = true;
            this._select(namespaceString, infoName);
        });
        let nextButton = new Gtk.Button({
            hasFrame: false, iconName: 'go-next-symbolic',
            sensitive: false, tooltipText: "Next",
        });
        nextButton.connect('clicked', () => {
            let { namespaceString, infoName } = this._history[++this._history.currentIndex];
            this._history.locked = true;
            this._select(namespaceString, infoName);
        });
        let viewButton = new Gtk.ToggleButton({
            hasFrame: false, iconName: 'tool-text-symbolic',
            sensitive: false, active: true,
        });

        let giView = new SourceView();
        giView.connect('link-activated', (giView_, namespaceName, infoName) => {
            let currentRow = this._listView.model.selectedItem.get_parent() ?? this._listView.model.selectedItem;

            let namespaceString = currentRow.item.string.split('-')[0] == namespaceName ?
                currentRow.item.string :
                Gir.Repository.get_default().get_dependencies(currentRow.item.string.split('-')[0])
                    .find(dependency => dependency.split('-')[0] == namespaceName);

            this._select(namespaceString, infoName);
        });

        let searchBox = new SearchBox({
            buffer: giView.buffer,
        });
        searchBox.connect('match-selected', () => {
            let [, insertIter] = giView.buffer.get_selection_bounds();
            if (giView.get_iter_location(insertIter).y < giView.get_visible_rect().y)
                giView.scroll_mark_onscreen(giView.buffer.get_insert());
            else
                giView.scroll_mark_onscreen(giView.buffer.get_selection_bound());
        });

        let filterEntry = new Gtk.SearchEntry();
        filterEntry.connect('search-changed', entry => {
            let row = this._listView.model.selectedItem;
            if (row.depth)
                return;

            let model = row?.expanded ? row.children : treeListModel.model;
            model.filter.search = entry.text;
        });

        let factory = new Gtk.SignalListItemFactory();
        factory.connect('setup', (factory_, listItem) => {
            listItem.child = new Gtk.TreeExpander({
                child: new Gtk.Label({
                    xalign: 0,
                    ellipsize: 3, // Pango.EllipsizeMode.END
                }),
            });
        });
        factory.connect('bind', (factory_, listItem) => {
            let listRow = listItem.item;
            let expander = listItem.child;

            expander.child.label = listRow.item.string;
            expander.listRow = listRow;
        });

        this._listView = new Gtk.ListView({
            factory,
            model: new Gtk.SingleSelection({
                model: treeListModel,
            }),
            cssClasses: ['view', 'navigation-sidebar'],
        });

        this._listView.model.connect('notify::selected-item', selection => {
            giView.buffer.text = '';

            let row = selection.selectedItem;
            if (!row)
                return;

            if (row.item instanceof Info)
                giView.showInfo(row.item);
            else
                giView.showToplevel(row.item);

            if (this._history.locked) {
                this._history.locked = false;
            } else {
                this._history.splice(++this._history.currentIndex);
                this._history[this._history.currentIndex] = { namespaceString: (row.get_parent() ?? row).item.string, infoName: row.item.base?.get_name() };
            }

            upButton.sensitive = row.item instanceof Info;
            prevButton.sensitive = this._history.currentIndex > 0;
            nextButton.sensitive = this._history.currentIndex < this._history.length - 1;
        });

        let listViewBox = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
        });
        listViewBox.append(filterEntry);
        listViewBox.append(new Gtk.ScrolledWindow({
            child: this._listView,
            hscrollbarPolicy: Gtk.PolicyType.NEVER,
            vexpand: true,
        }));

        let giViewHeader = new Gtk.CenterBox();
        giViewHeader.set_start_widget(new Gtk.Box());
        giViewHeader.get_start_widget().append(upButton);
        giViewHeader.get_start_widget().append(prevButton);
        giViewHeader.get_start_widget().append(nextButton);
        giViewHeader.set_end_widget(new Gtk.Box());
        giViewHeader.get_end_widget().append(viewButton);
        let giViewBox = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
        });
        giViewBox.append(giViewHeader);
        giViewBox.append(new Gtk.ScrolledWindow({
            child: giView,
            hexpand: true, vexpand: true,
        }));

        super._init(Object.assign({
            startChild: listViewBox,
            endChild: new Gtk.Overlay({
                child: giViewBox,
            }),
            position: 250,
        }, params));

        let searchSlider = new SearchSlider({
            halign: Gtk.Align.END,
            valign: Gtk.Align.START,
            child: searchBox,
            keyCaptureWidget: this,
            showCloseButton: true,
        });
        this.endChild.add_overlay(searchSlider);

        let cssProvider = new Gtk.CssProvider();
        cssProvider.load_from_data('textview.monospace { font-size: smaller; }');
        Gtk.StyleContext.add_provider_for_display(this.get_display(), cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
    }

    _select(namespaceString, infoName) {
        let namespaceIndex = [...this._namespaceList].findIndex(namespace => namespace.string == namespaceString);
        let treeListModel = this._listView.model.model;
        let namespaceRow = treeListModel.get_child_row(namespaceIndex);
        let namespacePosition = namespaceRow.get_position();

        if (infoName) {
            namespaceRow.set_expanded(true);
            let namespaceLength = namespaceRow.get_children().get_n_items();
            for (let i = namespacePosition + 1; i < namespacePosition + 1 + namespaceLength; i++)
                if (this._listView.model.get_item(i).item.base.get_name() == infoName) {
                    this._listView.activate_action('list.scroll-to-item', GLib.Variant.new_uint32(i));
                    this._listView.model.selected = i;
                    break;
                }
        } else {
            this._listView.activate_action('list.scroll-to-item', GLib.Variant.new_uint32(namespacePosition));
            this._listView.model.selected = namespacePosition;
        }
    }

    start() {
        let searchPath = Gir.Repository.get_search_path();
        let namespaces = [];
        searchPath.forEach(path => {
            let directory = Gio.File.new_for_path(path);
            try {
                let info, enumerator = directory.enumerate_children('standard::*', Gio.FileQueryInfoFlags.NONE, null);
                while ((info = enumerator.next_file(null))) {
                    let namespace = info.get_name().match(/(\w+-.+)\.typelib/)?.[1];
                    if (namespace && !namespaces.includes(namespace))
                        namespaces.push(namespace);
                }
                enumerator.close(null);
            } catch(e) {}
        });

        this._namespaceList.splice(0, 0, namespaces.sort((a, b) => a.toLowerCase() > b.toLowerCase()).map(string => new NamespaceObject({ string })));
    }
});
