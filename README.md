# GroundJS
A GJS playground.

## Setup
Initialize Evaluator submodule:

* `git submodule update --init`

### Run
GroundJS is launchable without any installation:

* Copy (or link) GJS [doc][gjs-doc] and [examples][gjs-examples] directories into `data/gjs/`.
* Copy (or link) GJS Guide [docs][gjs-guide] directory into `data/gjs-guide/`.

```sh
gjs ground-js.js
```

### Install
```sh
meson build && cd build && ninja && sudo ninja install
```

* Copy (or link) GJS [doc][gjs-doc] and [examples][gjs-examples] directories into `<any data directory>/org.codeberg.som.GroundJS/gjs/`.
* Copy (or link) GJS Guide [docs][gjs-guide] directory into `<any data directory>/org.codeberg.som.GroundJS/gjs-guide/`.

### Uninstall
```sh
cd build && sudo ninja uninstall
```

## Evaluator
See [Evaluator](./Evaluator/README.md) for informations.

## Syntax highlighting
Syntax highlighting depends on the [highlight](https://www.andre-simon.de/doku/highlight/highlight.html) program.

![Evaluation page](./data/screenshots/evaluation.png)
![Modules page](./data/screenshots/modules.png)
![Doc page](./data/screenshots/doc.png)
![Guide page](./data/screenshots/guide.png)
![Introspection page](./data/screenshots/introspection.png)

[gjs-doc]: https://gitlab.gnome.org/GNOME/gjs/tree/HEAD/doc
[gjs-examples]: https://gitlab.gnome.org/GNOME/gjs/tree/HEAD/examples
[gjs-guide]: https://gitlab.gnome.org/ewlsh/gjs-guide/tree/HEAD/docs
